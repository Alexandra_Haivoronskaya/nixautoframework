package nix.training.framework.spec

import geb.spock.GebReportingSpec
import nix.training.framework.page.BlogPage
import nix.training.framework.page.StartPage


class NixNavigationSpec extends GebReportingSpec {

    def "Navigate to Blog page"() {
        when:
            to StartPage
        
        and:
            "User navigates to Blog page"()

        then:
            BlogPage
    }
}